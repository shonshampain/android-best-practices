Best Practices Blue Sky Android Application
by Shon Shampain, shon.shampain@gmail.com

This is a simple app to demonstrate Android Best Practices, especially architecture.
There are two tabs on the Main Activity, connected by bottom navigation.
The first tab is the search tab, and displays the first result after you tap "GO".
The second tab is the history tab.

The app is tested on a Google Pixel 5 emulator running Android API 31.

Please note the following best practices:
-----------------------------------------
1. The app uses the MVVM architecture.
2. The app uses reactive patterns via CoRoutines/Flows and LiveData.
3. The app uses Hilt dependency injection.
4. The app uses Jetpack Compose and Jetpack Navigation.
5. ViewModels are associated with each Composition, and where applicable, activity.
6. ViewModels handle 2 things:
   i) Maintain the view state
   ii) A gateway to the Use Cases
7. All business logic is in tight, focused Use Cases
8. All communications to "outside" data (DB, APIs, etc.) is via Repos
9. All ViewModels, UseCases and Repos have unit tests
10. Repos are packaged with Fakes, b/c tests should not go off-device
    i) Fakes are bootstrapped into the test environment by loading different DI modules
    ii) Fakes are used as opposed to Mocks, here is why:
    https://www.reddit.com/r/androiddev/comments/d05di8/do_you_use_the_impl_suffix_if_you_only_have_one/ez86bi3/
11. The app supports testing robots for human readable integration tests.
    If you have never seen a human readable integration test, check out MainTest.kt at least.
    Human readable integration tests allow QA or other resources to easily write tests,
    while the Devs concentrate on the robots.
12. There are many other subtle nuances to the Application which make the architecture world class.
    We can discuss.
   
NB: To run the unit tests, right click app/src/test/java/com and choose Run "tests in 'com"
NB: To run the integration tests, right click app/src/androidTest/java/com and choose Run "tests in 'com"

Considering...
--------------
1. MVI Architecture
2. GraphQL
3. Fix integration tests to run w/ Compose
