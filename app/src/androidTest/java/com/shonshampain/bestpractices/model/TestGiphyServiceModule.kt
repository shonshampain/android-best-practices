package com.shonshampain.bestpractices.model

import com.shonshampain.bestpractices.model.giphy.FakeGiphyService
import com.shonshampain.bestpractices.model.giphy.GiphyService
import dagger.Module
import dagger.Provides
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn

@Module
@TestInstallIn(
    components = [SingletonComponent::class],
    replaces = [GiphyServiceModule::class]
)
object TestGiphyServiceModule {

    @Provides
    fun provideGiphyService(): GiphyService {
        return FakeGiphyService()
    }
}