package com.shonshampain.bestpractices

import android.app.Activity
import android.app.Application
import android.app.Application.ActivityLifecycleCallbacks
import android.os.Bundle
import dagger.hilt.android.testing.CustomTestApplication

var currentActivity: Activity = Activity()

@Suppress("unused") // Definitely used
@CustomTestApplication(TestBestPracticesApp::class) interface HiltTestApplication
open class TestBestPracticesApp : Application(), ActivityLifecycleCallbacks {

    override fun onCreate() {
        super.onCreate()
        registerActivityLifecycleCallbacks(this)
    }

    override fun onActivityPaused(activity: Activity) { }

    override fun onActivityResumed(activity: Activity) {
        currentActivity = activity
    }

    override fun onActivityStarted(activity: Activity) { }

    override fun onActivityDestroyed(activity: Activity) { }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) { }

    override fun onActivityStopped(activity: Activity) { }

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) { }
}