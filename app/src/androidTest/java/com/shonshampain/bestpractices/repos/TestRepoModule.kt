package com.shonshampain.bestpractices.repos

import dagger.Binds
import dagger.Module
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import javax.inject.Singleton

@Module
@TestInstallIn(
    components = [SingletonComponent::class],
    replaces = [RepoModule::class]
)
abstract class TestRepoModule {
    @Binds
    @Singleton
    abstract fun bindsGiphyRepo(
        giphyRepoImpl: GiphyRepoImpl
    ): GiphyRepo
    @Binds
    @Singleton
    abstract fun bindsSearchHistoryRepo(
        searchHistoryRepoImpl: FakeSearchHistoryRepoImpl
    ): SearchHistoryRepo
}