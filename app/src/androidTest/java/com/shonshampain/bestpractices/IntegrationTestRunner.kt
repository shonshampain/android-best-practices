package com.shonshampain.bestpractices

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner

@Suppress("unused") // Definitely used in build.gradle
class IntegrationTestRunner : AndroidJUnitRunner() {

    @Throws(
        ClassNotFoundException::class,
        IllegalAccessException::class,
        InstantiationException::class
    )
    override fun newApplication(cl: ClassLoader, className: String, context: Context): Application {
        return super.newApplication(cl, HiltTestApplication_Application::class.java.name, context)
    }
}
