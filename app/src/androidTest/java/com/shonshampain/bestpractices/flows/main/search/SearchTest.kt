package com.shonshampain.bestpractices.flows.main.search

import com.shonshampain.bestpractices.BaseIntegrationTest
import com.shonshampain.bestpractices.flows.startApp
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Test

@HiltAndroidTest
class SearchTest : BaseIntegrationTest() {

    /**
     * Test Case:
     * ----------
     * 1. Start the app
     * 2. Type "Dog" into the edit text field
     * 3. Tap the "GO" button
     * 4. Verify an image shows up
     */

    @Test
    fun searchTest() {
        startApp {
            enterASearchTerm(composeTestRule, "Dog")
            go(composeTestRule)
            verifyImage(composeTestRule)
        }
    }
}