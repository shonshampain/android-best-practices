package com.shonshampain.bestpractices.flows.main.history

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertTextContains
import androidx.compose.ui.test.junit4.AndroidComposeTestRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performScrollToIndex
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.shonshampain.bestpractices.flows.main.MainActivity
import com.shonshampain.bestpractices.flows.main.search.SearchRobot

class HistoryRobot {

    fun verifySearchTerm(testRule: AndroidComposeTestRule<ActivityScenarioRule<MainActivity>, MainActivity>, entryNumber: Int, searchTerm: String) {
        testRule.waitForIdle()
        testRule.onNodeWithTag("HistoryViewLazyColumn").performScrollToIndex(entryNumber).assertTextContains(searchTerm)
    }

    fun chooseSearchView(testRule: AndroidComposeTestRule<ActivityScenarioRule<MainActivity>, MainActivity>, func: SearchRobot.() -> Unit): SearchRobot {
        testRule.waitForIdle()
        testRule.onNodeWithTag("SearchButton").performClick()
        return SearchRobot().apply { func() }
    }

    fun verifyHistoryView(testRule: AndroidComposeTestRule<ActivityScenarioRule<MainActivity>, MainActivity>) {
        testRule.waitForIdle()
        testRule.onNodeWithTag("HistoryViewLayout").assertIsDisplayed()
    }
}