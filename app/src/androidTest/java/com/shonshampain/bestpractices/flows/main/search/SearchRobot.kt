package com.shonshampain.bestpractices.flows.main.search

import androidx.compose.ui.input.key.KeyEvent
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.AndroidComposeTestRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performKeyPress
import androidx.compose.ui.test.performTextInput
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.shonshampain.bestpractices.flows.main.MainActivity
import com.shonshampain.bestpractices.flows.main.history.HistoryRobot

class SearchRobot {

    fun enterASearchTerm(testRule: AndroidComposeTestRule<ActivityScenarioRule<MainActivity>, MainActivity>, searchTerm: String) {
        testRule.waitForIdle()
        testRule.onNodeWithTag("SearchTermEntry").performTextInput(searchTerm)
    }

    fun enterAKeyCode(testRule: AndroidComposeTestRule<ActivityScenarioRule<MainActivity>, MainActivity>, code: Int) {
        testRule.waitForIdle()
        //testRule.onNodeWithTag("SearchTermEntry").performKeyPress(code)
    }

    fun focusSearchFieldWithCursorAtEnd(testRule: AndroidComposeTestRule<ActivityScenarioRule<MainActivity>, MainActivity>) {
        testRule.waitForIdle()
        testRule.onNodeWithTag("SearchTermEntry").performClick()
        //testRule.onNodeWithTag("SearchTermEntry").performKeyPress()
    }

    fun go(testRule: AndroidComposeTestRule<ActivityScenarioRule<MainActivity>, MainActivity>) {
        testRule.waitForIdle()
        testRule.onNodeWithTag("Go").performClick()
    }

    fun verifyImage(testRule: AndroidComposeTestRule<ActivityScenarioRule<MainActivity>, MainActivity>) {
        testRule.waitForIdle()
        //waitForImageToLoad(R.id.search_image) TODO!!
    }

    fun verifySearchView(testRule: AndroidComposeTestRule<ActivityScenarioRule<MainActivity>, MainActivity>) {
        testRule.waitForIdle()
        testRule.onNodeWithTag("SearchViewLayout").assertIsDisplayed()
    }

    fun chooseHistoryView(testRule: AndroidComposeTestRule<ActivityScenarioRule<MainActivity>, MainActivity>, func: HistoryRobot.() -> Unit): HistoryRobot {
        testRule.waitForIdle()
        testRule.onNodeWithTag("HistoryButton").performClick()
        return HistoryRobot().apply { func() }
    }
}