package com.shonshampain.bestpractices.flows.main.history

import android.view.KeyEvent
import com.shonshampain.bestpractices.BaseIntegrationTest
import com.shonshampain.bestpractices.flows.startApp
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Test

@HiltAndroidTest
class HistoryTestWithSwiping : BaseIntegrationTest() {

    /**
     * Test Case:
     * ----------
     * 1. Start the app
     * 2. Type "Dog" into the search edit text
     * 3. Tap the "GO" button
     * 4. Choose the History view
     * 5. Verify that there is 1 search term "Dog"
     * 6. Swipe right
     * 7. Tap into the search edit text with the cursor at the end
     * 8. Tap DEL, DEL
     * 9. Tap the "GO" button
     * 10. Swipe left
     * 11. Verify that entry 1 is "D"
     */

    @Test
    fun historyTestWithSwiping() {
        startApp {
            enterASearchTerm(composeTestRule, "Dog")
            go(composeTestRule)
            chooseHistoryView(composeTestRule) {
                verifySearchTerm(composeTestRule, 0, "1 - Dog")
                chooseSearchView(composeTestRule) {
                    focusSearchFieldWithCursorAtEnd(composeTestRule)
                    enterAKeyCode(composeTestRule, KeyEvent.KEYCODE_DEL)
                    enterAKeyCode(composeTestRule, KeyEvent.KEYCODE_DEL)
                    go(composeTestRule)
                    chooseHistoryView(composeTestRule) {
                        verifySearchTerm(composeTestRule, 1, "2 - D")
                    }
                }
            }
        }
    }
}