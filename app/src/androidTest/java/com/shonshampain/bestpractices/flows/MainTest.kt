package com.shonshampain.bestpractices.flows

import com.shonshampain.bestpractices.BaseIntegrationTest
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Test

@HiltAndroidTest
class MainTest : BaseIntegrationTest() {

    /**
     * Test Case:
     * ----------
     * 1. Start the app
     * 2. Verify we are on the Search view
     * 3. Choose the History view
     * 4. Verify we are on the History tab
     * 5. Choose the Search view
     * 6. Verify we are on the Search view
     */

    @Test
    fun mainTest() {
        startApp() {
            verifySearchView(composeTestRule)
            chooseHistoryView(composeTestRule) {
                verifyHistoryView(composeTestRule)
                chooseSearchView(composeTestRule) {
                    verifySearchView(composeTestRule)
                }
            }
        }
    }
}