package com.shonshampain.bestpractices.flows

import com.shonshampain.bestpractices.flows.MainTabs.HISTORY
import com.shonshampain.bestpractices.flows.MainTabs.SEARCH
import com.shonshampain.bestpractices.flows.main.search.SearchRobot

enum class MainTabs { SEARCH, HISTORY }

val mainTabsByName = mapOf(
    SEARCH to "SearchFragment",
    HISTORY to "HistoryFragment"
)

fun startApp(func: SearchRobot.() -> Unit): SearchRobot {
    return SearchRobot().apply { func() }
}

class MainRobot