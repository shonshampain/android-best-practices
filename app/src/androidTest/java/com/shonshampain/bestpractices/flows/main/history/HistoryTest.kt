package com.shonshampain.bestpractices.flows.main.history

import com.shonshampain.bestpractices.BaseIntegrationTest
import com.shonshampain.bestpractices.flows.startApp
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Test

@HiltAndroidTest
class HistoryTest : BaseIntegrationTest() {

    /**
     * Test Case:
     * ----------
     * 1. Start the app
     * 2. Type "Dog" into the search edit text
     * 3. Tap "GO"
     * 4. Choose the History view
     * 5. Verify that there is 1 search term, "Dog"
     */

    @Test
    fun historyTest() {
        startApp {
            enterASearchTerm(composeTestRule, "Dog")
            go(composeTestRule)
            chooseHistoryView(composeTestRule) {
                verifySearchTerm(composeTestRule, 0, "1 - Dog")
            }
        }
    }
}