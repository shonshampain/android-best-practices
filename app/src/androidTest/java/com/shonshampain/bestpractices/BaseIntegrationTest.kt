package com.shonshampain.bestpractices

import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.test.rule.ActivityTestRule
import com.shonshampain.bestpractices.flows.main.MainActivity
import dagger.hilt.android.testing.HiltAndroidRule
import org.junit.After
import org.junit.Before
import org.junit.Rule


open class BaseIntegrationTest {

    @Rule(order = 0)
    @JvmField
    var hiltRule = HiltAndroidRule(this)

    @Rule(order = 1)
    @JvmField
    var activityTestRule = ActivityTestRule(MainActivity::class.java, true, false)

    @get:Rule(order = 2)
    var composeTestRule = createAndroidComposeRule<MainActivity>()

    @Before
    fun startup() {
        activityTestRule.launchActivity(null)
    }

    @After
    fun teardown() {
        activityTestRule.finishActivity()
    }
}