package com.shonshampain.bestpractices.usecases

import com.shonshampain.bestpractices.model.giphy.GiphySearchResponse
import com.shonshampain.bestpractices.repos.GiphyRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

interface GiphyUseCase {
    suspend fun search(term: String)
    val giphyResult : Flow<Result<GiphySearchResponse>>
}

class GiphyUseCaseImpl @Inject constructor(
    private val giphyRepo: GiphyRepo
) : GiphyUseCase {

    override val giphyResult : Flow<Result<GiphySearchResponse>> = giphyRepo.giphyResult

    override suspend fun search(term: String) {
        giphyRepo.search(term)
    }
}
