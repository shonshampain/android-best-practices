package com.shonshampain.bestpractices.usecases

import com.shonshampain.bestpractices.model.search.SearchHistoryResponse
import com.shonshampain.bestpractices.repos.SearchHistoryRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

interface SearchHistoryUseCase {
    suspend fun loadSearchHistory()
    suspend fun addToSearchHistory(searchTerm: String)
    var searchHistory : Flow<Result<SearchHistoryResponse>>
}

class SearchHistoryUseCaseImpl @Inject constructor(
    private val searchHistoryRepo: SearchHistoryRepo
) : SearchHistoryUseCase {

    override var searchHistory : Flow<Result<SearchHistoryResponse>> = searchHistoryRepo.searchHistory

    override suspend fun loadSearchHistory() {
        searchHistoryRepo.addToSearchHistory()
    }

    override suspend fun addToSearchHistory(searchTerm: String) {
        searchHistoryRepo.addToSearchHistory(searchTerm)
    }
}
