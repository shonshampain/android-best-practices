package com.shonshampain.bestpractices.usecases

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class UseCaseModule {
    @Binds
    @Singleton
    abstract fun bindsGiphyUseCase(
        giphyUseCaseImpl: GiphyUseCaseImpl
    ): GiphyUseCase
    @Binds
    @Singleton
    abstract fun bindsSearchHistoryUseCase(
        searchHistoryUseCaseImpl: SearchHistoryUseCaseImpl
    ): SearchHistoryUseCase
}