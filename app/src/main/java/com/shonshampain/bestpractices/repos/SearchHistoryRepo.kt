package com.shonshampain.bestpractices.repos

import android.content.Context
import com.shonshampain.bestpractices.model.search.History
import com.shonshampain.bestpractices.model.search.HistoryDatabase
import com.shonshampain.bestpractices.model.search.SearchHistoryResponse
import com.shonshampain.bestpractices.utility.emitWithStatus
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import java.util.Date
import javax.inject.Inject

interface SearchHistoryRepo {
    suspend fun addToSearchHistory(searchTerm: String = "")
    val searchHistory : Flow<Result<SearchHistoryResponse>>
}

class SearchHistoryRepoImpl @Inject constructor(
    @ApplicationContext applicationContext: Context
) : SearchHistoryRepo {

    private val db = HistoryDatabase.getInstance(applicationContext)

    override val searchHistory = MutableSharedFlow<Result<SearchHistoryResponse>>()

    private suspend fun queryAll(): SearchHistoryResponse =
        SearchHistoryResponse(db?.historyDataDao()?.all() ?: listOf())

    private suspend fun insert(searchTerm: String) =
        db?.historyDataDao()?.insert(History(searchTerm = searchTerm, timestamp = Date().time))

    override suspend fun addToSearchHistory(searchTerm: String) {
        searchHistory.emit(
            emitWithStatus {
                if (searchTerm.isNotEmpty()) insert(searchTerm)
                queryAll()
            }
        )
    }
}

class FakeSearchHistoryRepoImpl @Inject constructor(): SearchHistoryRepo {

    var id = 1L
    var timestamp = 0L
    val history = mutableListOf<History>()
    override val searchHistory = MutableSharedFlow<Result<SearchHistoryResponse>>()

    override suspend fun addToSearchHistory(searchTerm: String) {
        if (searchTerm.isNotEmpty()) {
            history += History(id++, searchTerm, timestamp++)
        }
        searchHistory.emit(
            emitWithStatus {
                SearchHistoryResponse(history)
            }
        )
    }
}