package com.shonshampain.bestpractices.repos

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepoModule {
    @Binds
    @Singleton
    abstract fun bindsGiphyRepo(
        giphyRepoImpl: GiphyRepoImpl
    ): GiphyRepo
    @Binds
    @Singleton
    abstract fun bindsSearchHistoryRepo(
        searchHistoryRepoImpl: SearchHistoryRepoImpl
    ): SearchHistoryRepo
}