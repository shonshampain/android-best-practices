package com.shonshampain.bestpractices.repos

import com.shonshampain.bestpractices.model.giphy.GiphySearchResponse
import com.shonshampain.bestpractices.model.giphy.GiphyService
import com.shonshampain.bestpractices.utility.emitWithStatus
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import javax.inject.Inject

interface GiphyRepo {
    suspend fun search(term: String)
    val giphyResult : Flow<Result<GiphySearchResponse>>
}

class GiphyRepoImpl @Inject constructor(
    private val giphyService: GiphyService
) : GiphyRepo {

    override val giphyResult = MutableSharedFlow<Result<GiphySearchResponse>>()

    override suspend fun search(term: String) {
        giphyResult.emit(
            emitWithStatus {
                giphyService.search(q = term)
            }
        )
    }
}
