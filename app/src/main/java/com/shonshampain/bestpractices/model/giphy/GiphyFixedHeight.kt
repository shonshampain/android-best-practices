package com.shonshampain.bestpractices.model.giphy

data class GiphyFixedHeight(
    val url: String
)