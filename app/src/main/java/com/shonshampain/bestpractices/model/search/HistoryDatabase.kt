package com.shonshampain.bestpractices.model.search

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [History::class], version = 1, exportSchema = false)
abstract class HistoryDatabase : RoomDatabase() {

    abstract fun historyDataDao(): HistoryDAO

    companion object {
        private var INSTANCE: HistoryDatabase? = null

        fun getInstance(context: Context): HistoryDatabase? {
            if (INSTANCE == null) {
                synchronized(HistoryDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context,
                        HistoryDatabase::class.java, "history.db")
                        .build()
                }
            }
            return INSTANCE
        }

        @Suppress("unused")
        fun destroyInstance() {
            INSTANCE = null
        }
    }
}