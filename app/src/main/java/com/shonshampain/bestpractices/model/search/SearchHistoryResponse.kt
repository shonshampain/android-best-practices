package com.shonshampain.bestpractices.model.search

data class SearchHistoryResponse(
    val list: List<History> = listOf()
)