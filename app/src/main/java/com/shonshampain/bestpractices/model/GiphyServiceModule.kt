package com.shonshampain.bestpractices.model

import com.shonshampain.bestpractices.model.giphy.GiphyService
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object GiphyServiceModule {

    @Provides
    @Singleton
    fun provideGiphyService(okHttpClient: OkHttpClient, gson: Gson): GiphyService {
        return Retrofit.Builder()
            .baseUrl(GiphyService.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(GiphyService::class.java)
    }
}