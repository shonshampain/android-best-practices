package com.shonshampain.bestpractices.model.giphy

import retrofit2.http.GET
import retrofit2.http.Query

private const val GIPHY_API_KEY = "mT4Ll9X3GZBuuI3I43MuoEWwDv33Kl1m"

interface GiphyService {

    companion object {
        const val BASE_URL = "https://api.giphy.com/"
    }

    @GET("/v1/gifs/search")
    suspend fun search(
        @Query("q") q: String,
        @Query("limit") limit: Int = 1,
        @Query("api_key") apiKey: String = GIPHY_API_KEY
    ): GiphySearchResponse
}

enum class FakeGiphyApiManagerResponses { SUCCESS, EMPTY_LIST, ERROR }

class FakeGiphyService(private val response: FakeGiphyApiManagerResponses = FakeGiphyApiManagerResponses.SUCCESS) : GiphyService {

    override suspend fun search(q: String, limit: Int, apiKey: String): GiphySearchResponse {
        return when (response) {
            FakeGiphyApiManagerResponses.SUCCESS -> {
                val fh = GiphyFixedHeight(url = "url-url-url")
                val images = GiphyImages(fh)
                val gd = listOf(GiphyData(images))
                val gsr = GiphySearchResponse(gd)
                gsr
            }
            FakeGiphyApiManagerResponses.EMPTY_LIST -> {
                GiphySearchResponse(listOf())
            }
            FakeGiphyApiManagerResponses.ERROR -> {
                throw Exception("some kind of error")
            }
        }
    }
}