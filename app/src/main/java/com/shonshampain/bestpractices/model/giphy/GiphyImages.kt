package com.shonshampain.bestpractices.model.giphy

data class GiphyImages(
    val fixed_height: GiphyFixedHeight
)