package com.shonshampain.bestpractices.model.giphy

data class GiphySearchResponse(
    val data: List<GiphyData> = listOf()
)