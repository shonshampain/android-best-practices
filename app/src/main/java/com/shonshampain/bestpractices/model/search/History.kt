package com.shonshampain.bestpractices.model.search

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "history")
data class History(

    @PrimaryKey(autoGenerate = true)
    val id: Long? = null,

    @ColumnInfo(name = "search_term")
    val searchTerm: String,

    @ColumnInfo(name = "timestamp")
    val timestamp: Long
)