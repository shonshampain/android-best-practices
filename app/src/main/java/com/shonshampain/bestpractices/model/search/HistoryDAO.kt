package com.shonshampain.bestpractices.model.search

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query

@Dao
interface HistoryDAO {

    @Query("SELECT * from history")
    suspend fun all(): List<History>

    @Insert(onConflict = REPLACE)
    suspend fun insert(history: History)

    @Delete
    suspend fun delete(history: History)
}