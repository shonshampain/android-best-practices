package com.shonshampain.bestpractices.model.giphy

data class GiphyData(
    val images: GiphyImages
)