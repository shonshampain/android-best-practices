package com.shonshampain.bestpractices.utility

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

fun Context.hideKeyboard() {
    val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    // Find the currently focused view, so we can grab the correct window token from it.
    var view = (this as? Activity)?.currentFocus
    // If no view currently has focus, create a new one, just so we can grab a window token from it
    if (view == null) {
        view = View(this)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

suspend inline fun <reified T> emitWithStatus(crossinline value: suspend () -> T): Result<T> {
    return try {
        Result.success(value())
    } catch (e: Exception) {
        Result.failure(e)
    }
}