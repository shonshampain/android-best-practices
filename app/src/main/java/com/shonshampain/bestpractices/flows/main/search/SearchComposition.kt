package com.shonshampain.bestpractices.flows.main.search

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material.Button
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import coil.ImageLoader
import coil.compose.rememberAsyncImagePainter
import com.shonshampain.bestpractices.R

class SearchComposition(
    private val vm: SearchViewModel,
    private val imageLoader: ImageLoader
) {

    @Composable
    fun Search() {
        val state by vm.imageState.collectAsState()
        SearchViewLayout(state)
    }

    @Composable
    private fun SearchViewLayout(imageState: ImageState) {
        Scaffold(
            modifier = Modifier.testTag("SearchViewLayout")
        ) {
            Column(
                modifier = Modifier.padding(it)
            ) {
                Row(
                    modifier = Modifier.height(IntrinsicSize.Min),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    SearchTermEntry()
                    Spacer(modifier = Modifier.width(16.dp))
                    SearchButton()
                }
                Row {
                    ErrorStatus(imageState.error)
                }
                Row {
                    SearchImage(imageState.url)
                }
            }
        }
    }

    @Composable
    private fun SearchTermEntry() {
        val context = LocalContext.current
        var text by remember { mutableStateOf(TextFieldValue("")) }
        OutlinedTextField(
            value = text,
            onValueChange = {
                text = it
                vm.searchTerm = text.text
            },
            label = { Text(text = context.getString(R.string.search_term_hint)) },
            placeholder = { Text(text = context.getString(R.string.search_term_placeholder)) },
            modifier = Modifier
                .testTag("SearchTermEntry")
                .wrapContentWidth()
                .wrapContentHeight(),
        )
    }

    @Composable
    fun SearchButton() {
        val context = LocalContext.current
        Button(
            modifier = Modifier.testTag("Go").fillMaxWidth(),
            onClick = {
                vm.search()
                vm.hideKeyboard(context)
            }
        ) {
            Text(text = context.getString(R.string.go))
        }
    }

    @Composable
    private fun ErrorStatus(errorMessage: String?) {
        errorMessage?.let {
            Text(text = errorMessage, color = Color.White)
        }
    }

    @Composable
    private fun SearchImage(imageUrl: String = "") {
        val context = LocalContext.current
        if (imageUrl.isNotEmpty()) {
            Image(
                modifier = Modifier
                    .fillMaxHeight()
                    .fillMaxWidth(),
                painter = rememberAsyncImagePainter(imageUrl, imageLoader = imageLoader),
                contentDescription = context.getString(R.string.search_image_content_description)
            )
        }
    }
}
