package com.shonshampain.bestpractices.flows.main

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.BottomAppBar
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.darkColors
import androidx.compose.material.primarySurface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import coil.ImageLoader
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.shonshampain.bestpractices.R
import com.shonshampain.bestpractices.flows.main.history.HistoryComposition
import com.shonshampain.bestpractices.flows.main.history.HistoryViewModel
import com.shonshampain.bestpractices.flows.main.search.SearchComposition
import com.shonshampain.bestpractices.flows.main.search.SearchViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

private const val SEARCH_ROUTE = "search"
private const val HISTORY_ROUTE = "history"

sealed class Screen(val route: String) {
    object Search: Screen(route = SEARCH_ROUTE)
    object History: Screen(route = HISTORY_ROUTE)
}

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    @Inject
    lateinit var imageLoader: ImageLoader

    private val searchVm: SearchViewModel by viewModels()
    private val searchComposition by lazy { SearchComposition(searchVm, imageLoader) }

    private val historyVm: HistoryViewModel by viewModels()
    private val historyComposition by lazy { HistoryComposition(historyVm) }

    private lateinit var navController: NavHostController

    private val blue300 = Color(0xff64b5f6)
    private val blueGray400 = Color(0xff78909c)
    private val appColors = darkColors(
        primary = blue300,
        secondary = blueGray400,
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SetSystemBarColor()
            MaterialTheme(
                colors = appColors
            ) {
                MainNavView()
            }
        }
    }

    @Composable
    private fun SetSystemBarColor() {
        val systemUiController = rememberSystemUiController()
        systemUiController.setSystemBarsColor(
            color = appColors.primarySurface
        )
    }

    @Composable
    private fun MainNavView() {
        Scaffold(
            bottomBar = { bottom() },
            topBar = { top() },
        ) {
            navController = rememberNavController()
            NavHost(
                navController = navController,
                startDestination = "search",
                modifier = Modifier.padding(it)
            ) {
                composable(SEARCH_ROUTE) { searchComposition.Search() }
                composable(HISTORY_ROUTE) { historyComposition.History() }
            }
        }
    }

    @Composable
    private fun top() {
        TopAppBar {
            Spacer(Modifier.size(16.dp))
            Text(text = getString(R.string.app_name))
        }
    }

    @Composable
    private fun bottom() {
        BottomAppBar {
            Spacer(Modifier.size(16.dp))
            Button(
                onClick = { navController.navigate(Screen.Search.route) },
                modifier = Modifier.testTag("SearchButton"),
            ) {
                Text(getString(R.string.search))
            }
            Spacer(Modifier.size(16.dp))
            Button(
                onClick = { navController.navigate(Screen.History.route) },
                modifier = Modifier.testTag("HistoryButton")
            ) {
                Text(getString(R.string.history))
            }
        }
    }

    @Preview
    @Composable
    private fun previewMainNavView() {
        MaterialTheme(
            colors = appColors
        ) {
            MainNavView()
        }
    }
}
