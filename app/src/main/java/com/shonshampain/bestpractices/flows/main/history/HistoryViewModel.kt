package com.shonshampain.bestpractices.flows.main.history

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.shonshampain.bestpractices.model.search.History
import com.shonshampain.bestpractices.usecases.SearchHistoryUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

data class HistoryState(
    val history: List<History> = listOf()
)

@HiltViewModel
class HistoryViewModel @Inject constructor(
    private val searchHistoryUseCase: SearchHistoryUseCase
) : ViewModel() {

    private val _historyState = MutableStateFlow(HistoryState())
    val historyState: StateFlow<HistoryState>
        get() = _historyState.asStateFlow()

    init {
        viewModelScope.launch {
            searchHistoryUseCase.searchHistory.collect {
                val returnedVal = it.getOrNull()?.list
                if (it.isSuccess && returnedVal?.isNotEmpty() == true) {
                    updateHistoryList(returnedVal)
                } else {
                    if (it.isFailure) {
                        // TODO
                    } else if (returnedVal?.isEmpty() == true) {
                        // TODO
                    }
                }
            }
        }
        viewModelScope.launch {
            searchHistoryUseCase.loadSearchHistory()
        }
    }

    private fun updateHistoryList(list: List<History>) {
        _historyState.value = historyState.value.copy(history = list)
    }
}