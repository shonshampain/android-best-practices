package com.shonshampain.bestpractices.flows.main.search

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.shonshampain.bestpractices.usecases.GiphyUseCase
import com.shonshampain.bestpractices.usecases.SearchHistoryUseCase
import com.shonshampain.bestpractices.utility.hideKeyboard
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

data class ImageState(
    val error: String = "",
    val url: String = ""
)

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val giphyUseCase: GiphyUseCase,
    private val searchHistoryUseCase: SearchHistoryUseCase
) : ViewModel() {

    var searchTerm = ""

    private val _imageState = MutableStateFlow(ImageState())
    val imageState: StateFlow<ImageState>
        get() = _imageState.asStateFlow()

    init {
        viewModelScope.launch {
            giphyUseCase.giphyResult.collect {
                    val returnedValue = it.getOrNull()?.data
                    if (it.isSuccess && returnedValue?.isNotEmpty() == true) {
                        updateImageUrl(returnedValue[0].images.fixed_height.url)
                    } else {
                        if (it.isFailure) {
                            updateErrorStatus(it.exceptionOrNull()?.message ?:
                                "Something went wrong, please try again.")
                        } else {
                            updateErrorStatus("Your search returned no results.")
                        }
                    }
                }
        }
    }

    private fun updateImageUrl(updatedUrl: String) {
        _imageState.value = imageState.value.copy(url = updatedUrl, error = "")
    }

    private fun updateErrorStatus(message: String) {
        _imageState.value = imageState.value.copy(url = "", error = message)
    }

    fun search() {
        val term = searchTerm.trim()
        if (term.isNotEmpty()) {
            performSearch(term)
            updateSearchTerms(term)
        }
    }

    fun hideKeyboard(context: Context) {
        context.hideKeyboard()
    }

    private fun performSearch(term: String) {
        Timber.d("[search] searching for $term")
        viewModelScope.launch {
            giphyUseCase.search(term)
        }
    }

    private fun updateSearchTerms(term: String) {
        Timber.d("[search] updating search terms to add: $term")
        viewModelScope.launch(Dispatchers.IO) {
            searchHistoryUseCase.addToSearchHistory(term)
        }
    }
}