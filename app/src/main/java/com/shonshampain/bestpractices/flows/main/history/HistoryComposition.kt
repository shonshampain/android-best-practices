package com.shonshampain.bestpractices.flows.main.history

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.MaterialTheme.typography
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.unit.dp
import com.shonshampain.bestpractices.R
import com.shonshampain.bestpractices.model.search.History

class HistoryComposition(
    private val vm: HistoryViewModel
) {

    @Composable
    fun History() {
        val state by vm.historyState.collectAsState()
        HistoryViewLayout(state)
    }

    @Composable
    private fun HistoryViewLayout(state: HistoryState) {
        Scaffold(
            modifier = Modifier.padding(16.dp).testTag("HistoryViewLayout")
        ) {
            LazyColumn(
                modifier = Modifier.padding(it).testTag("HistoryViewLazyColumn")
            ) {
                items(state.history) { data ->
                    HistoryItem(item = data)
                }
            }
        }
    }

    @Composable
    fun HistoryItem(item: History) {
        val context = LocalContext.current
        Row {
            Column {
                Text(
                    text = context.getString(R.string.history_display, item.id, item.searchTerm),
                    style = typography.h6,
                    color = Color.White
                )
            }
        }
    }
}
