package com.shonshampain.bestpractices.flows.main.search

import com.shonshampain.bestpractices.BaseUnitTest
import com.shonshampain.bestpractices.model.giphy.FakeGiphyApiManagerResponses
import com.shonshampain.bestpractices.model.giphy.FakeGiphyService
import com.shonshampain.bestpractices.model.search.History
import com.shonshampain.bestpractices.model.search.SearchHistoryResponse
import com.shonshampain.bestpractices.repos.FakeSearchHistoryRepoImpl
import com.shonshampain.bestpractices.repos.GiphyRepoImpl
import com.shonshampain.bestpractices.usecases.GiphyUseCaseImpl
import com.shonshampain.bestpractices.usecases.SearchHistoryUseCaseImpl
import com.google.common.truth.Truth.assertThat
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Test
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest

class SearchViewModelTest : BaseUnitTest() {

    private lateinit var underTest: SearchViewModel

    @Before
    override fun startup() {
        super.startup()
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @Test
    fun `on success search should return a url in the image state`() =
        runTest {
            // Given...
            underTest = SearchViewModel(GiphyUseCaseImpl(GiphyRepoImpl(FakeGiphyService())),
                SearchHistoryUseCaseImpl(FakeSearchHistoryRepoImpl()))
            underTest.searchTerm = "cheeseburger"

            // When...
            underTest.search()

            // Then...
            assertThat(underTest.imageState.value.url).isEqualTo("url-url-url")
        }

    @Test
    fun `on failure search should return an error message`()  =
        runTest {
            // Given...
            underTest = SearchViewModel(
                GiphyUseCaseImpl(
                    GiphyRepoImpl(
                        FakeGiphyService(
                            FakeGiphyApiManagerResponses.ERROR
                        )
                    )
                ),
                SearchHistoryUseCaseImpl(FakeSearchHistoryRepoImpl())
            )
            underTest.searchTerm = "cheeseburger"

            // When...
            underTest.search()

            // Then...
            assertThat(underTest.imageState.value.error).isEqualTo("some kind of error")
    }

    @Test
    fun `each search term should be added to the history`() =
        runTest {
            // Given...
            val searchHistoryRepo = FakeSearchHistoryRepoImpl()
            val searchHistoryUseCase = SearchHistoryUseCaseImpl(searchHistoryRepo)
            underTest = SearchViewModel(
                GiphyUseCaseImpl(GiphyRepoImpl(FakeGiphyService())),
                searchHistoryUseCase
            )
            val searchTerms = listOf("Foo", "Bar", "ABC", "123", "Ya", "Zoinks")

            // When... / Then...
            val progressiveList = mutableListOf<History>()
            var received: Result<SearchHistoryResponse>? = null
            var id = 1L
            var timestamp = 0L
            searchTerms.forEach {
                val job = launch(UnconfinedTestDispatcher(testScheduler)) {
                    received = searchHistoryRepo.searchHistory.first()
                }
                underTest.searchTerm = it
                underTest.search()
                progressiveList.add(History(id = id++, searchTerm = it, timestamp++))
                println(progressiveList)
                job.cancel()
                assertThat(received?.getOrNull()?.list).isEqualTo(progressiveList)
            }
        }

    @Test
    fun `an empty search term should not generate a search`() =
        runTest {
            // Given...
            underTest = SearchViewModel(
                GiphyUseCaseImpl(GiphyRepoImpl(FakeGiphyService())),
                SearchHistoryUseCaseImpl(FakeSearchHistoryRepoImpl())
            )
            val precondition = underTest.imageState.value

            // When...
            underTest.search()

            // Then...
            assertThat(underTest.imageState.value).isSameAs(precondition)
        }
}