package com.shonshampain.bestpractices.usecases

import com.shonshampain.bestpractices.BaseUnitTest
import com.shonshampain.bestpractices.model.giphy.FakeGiphyApiManagerResponses
import com.shonshampain.bestpractices.model.giphy.FakeGiphyService
import com.shonshampain.bestpractices.model.giphy.GiphySearchResponse
import com.shonshampain.bestpractices.repos.GiphyRepoImpl
import com.google.common.truth.Truth.assertThat
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Test

class GiphyUseCaseImplTest : BaseUnitTest() {

    private lateinit var underTest: GiphyUseCase

    @Before
    override fun startup() {
        super.startup()
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        Dispatchers.setMain(TestCoroutineDispatcher())
    }

    @Test
    fun `when the search has results the use case result should contain a url`() =
        runTest {
            // Given...
            underTest = GiphyUseCaseImpl(GiphyRepoImpl(FakeGiphyService()))

            // When...
            var flowResult: Result<GiphySearchResponse>? = null
            val job = launch(UnconfinedTestDispatcher(testScheduler)) {
                flowResult = underTest.giphyResult.first()
            }
            underTest.search("cheeseburger")
            job.cancel()

            // Then...
            assertThat(flowResult?.isSuccess).isTrue()
            assertThat(flowResult?.getOrNull()?.data?.get(0)?.images?.fixed_height?.url).isEqualTo("url-url-url")
        }

    @Test
    fun `when the search has no results the use case result should contain an empty list`() =
        runTest {
            // Given...
            underTest = GiphyUseCaseImpl(GiphyRepoImpl(FakeGiphyService(FakeGiphyApiManagerResponses.EMPTY_LIST)))

            // When...
            var flowResult: Result<GiphySearchResponse>? = null
            val job = launch(UnconfinedTestDispatcher(testScheduler)) {
                flowResult = underTest.giphyResult.first()
            }
            underTest.search("cheeseburger")
            job.cancel()

            // Then...
            assertThat(flowResult?.isSuccess).isTrue()
            assertThat(flowResult?.getOrNull()?.data?.size).isEqualTo(0)
        }

    @Test
    fun `when the use case returns an error the search result should be a failure with a reason`() =
        runTest {
            // Given...
            underTest = GiphyUseCaseImpl(GiphyRepoImpl(FakeGiphyService(FakeGiphyApiManagerResponses.ERROR)))

            // When...
            var flowResult: Result<GiphySearchResponse>? = null
            val job = launch(UnconfinedTestDispatcher(testScheduler)) {
                flowResult = underTest.giphyResult.first()
            }
            underTest.search("cheeseburger")
            job.cancel()

            // Then...
            assertThat(flowResult?.isSuccess).isFalse()
            assertThat(flowResult?.exceptionOrNull()?.message).isEqualTo("some kind of error")
        }
}