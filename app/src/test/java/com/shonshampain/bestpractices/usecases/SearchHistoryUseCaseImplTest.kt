package com.shonshampain.bestpractices.usecases

import com.shonshampain.bestpractices.BaseUnitTest
import com.shonshampain.bestpractices.model.search.History
import com.shonshampain.bestpractices.model.search.SearchHistoryResponse
import com.shonshampain.bestpractices.repos.FakeSearchHistoryRepoImpl
import com.google.common.truth.Truth.assertThat
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Test

class SearchHistoryUseCaseImplTest : BaseUnitTest() {

    private lateinit var underTest: SearchHistoryUseCase

    @Before
    override fun startup() {
        super.startup()
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        Dispatchers.setMain(Dispatchers.Unconfined)
        underTest = SearchHistoryUseCaseImpl(FakeSearchHistoryRepoImpl())
    }

    @Test
    fun `each additional search term should be reflected in the history`() {
        runTest {
            // Given...
            val searchTerms = listOf("Foo", "Bar", "ABC", "123", "Ya", "Zoinks")

            // When... / Then...
            val progressiveList = mutableListOf<History>()
            var id = 1L
            var timestamp = 0L
            searchTerms.forEach {
                var flowResult: Result<SearchHistoryResponse>? = null
                val job = launch(UnconfinedTestDispatcher(testScheduler)) {
                    flowResult = underTest.searchHistory.first()
                }
                underTest.addToSearchHistory(it)
                progressiveList.add(History(id = id++, searchTerm = it, timestamp = timestamp++))
                job.cancel()
                assertThat(flowResult?.getOrNull()?.list).isEqualTo(progressiveList)
            }
        }
    }
}